charm/tpl
========

Ultra minimalistic template engine. For those of use who don't like to spend a day setting up
a framework and everything else just to make tiny tool or feature. Works great with charm/orm
for simple database abstraction and charm/router for routing - but it needs neither.

I don't recommend PHP developers to learn yet another template language. The web is moving
toward APIs, so I decided to write the smallest most concise version of a template engine I
could - without sacrificing the nice things about blade and twig.

There really should be no limitation regarding how big projects you can use this for though.
The source code is quite simple. The challenge was in getting the output buffering right, and 
get an elegant structure.

See below for template file examples. This is how you would use the template engine:

```
    // TPL will proxy properties and methods from the $user instance in your template.
    $template = new TPL('user/profile', $user);

    echo $template->render(); // echo, send mail or whatever you want.
```

If you are really lazy, this also works. I don't recommmend it, and will probably remove the
feature because it relies on the __destruct() method.

```php
<?php
require('vendor/autoload.php');
new Charm\TPL('hello-world', ['current_time' => gmdate('Y-m-d H:i:s')]);
```




Installation & Setup
--------------------

Install the package:

```bash
composer require charm/tpl
```

Create a folder for template files:

```bash
mkdir templates
```

Make sure the following PHP code runs before you try to render a template:

```php
define APP_ROOT = '/var/www/html'; // Edit as appropriate
```

That's it! charm/tpl expects template files to have the extension `.tpl.php`.


A minimal master template file
------------------------------

We'll put this file in `templates/skeleton/html5.tpl.php`.

```php
<!DOCTYPE html>
<html>
    <head>
        <!-- $this->html() helps you NOT have to type htmlspecialchars($this->title, ENT_QUOTES) -->
        <title><?=$this->html("title"); ?></title>
    </head>
    <body>
        <main>
        <?=$this->body; ?>
        </main>

        <sidebar>
        <?=$this->sidebar; ?>
        </sidebar>

        <footer>
        <?php foreach ($this->footerBlocks as $block) { ?>
            <?=$block; ?>
        <?php } ?>

    </body>
</html>
```

Of course, everything is unrestricted PHP code, so you can do whatever you want.

A minimal child template file
-----------------------------

This file could be in `templates/index.tpl.php`.

```php
<?php $this->extend('skeleton/html5'); ?>

    <h1>Body Part</h1>
    <p>Whatever you output immediately after extend() will be used as the 'body' variable for the
    parent template.</p>

<?php $this->part('sidebar'); ?>

    <h2>Sidebar</h2>
    <p>This goes into the 'sidebar' variable of the parent.</p>

<?php $this->add('footerBlocks'); ?>

    <h3>Footer Block 1</h3>
    <p>Using 'add()' makes 'footerBlocks' become an array.</p>

<?php $this->add('footerBlocks'); ?>

    <?=$this->include('parts/footer-block', [ 'title' => 'Footer Block 2', 'text' => 'Some Text' ]); ?>

<?php $this->end(); ?>
```

A minimal reusable template part
--------------------------------

This file could be put in `templates/parts/footer-block.tpl.php'.

```php
<div class="footer-block">

    <h3><?=$this->html('title'); ?></h3>
    <div><?=$this->html('text'); ?></div>

</div>
```

