<?php
declare(strict_types=1);

namespace Charm;

/**
 * charm/tpl
 * =========
 *
 * Minimal template engine for PHP, using PHP as language with support
 * for template inheritance.
 *
 * Instructions:
 * -------------
 *
 * 1. Configure the `TPL::$template_root` according to wherever your template files
 *    are.
 *
 * 2. Name your template files 'path/to/some-template.tpl.php' in that path.
 *
 * 3. Write your template as a PHP file (opening and closing tags are different for
 *    obvious reasons in this file.
 *
 *      ```
 *      <%php
 *          $this->extend('skeleton/base');
 *
 *          // Set a variable in the template you are extending
 *          $this->part('title', 'Welcome to our website');
 *
 *          // Capture a variable to the template you are extending, by
 *          // writing HTML here
 *          $this->part('body');
 *      %>
 *
 *      <!-- goes to the 'body' -->
 *      <h1>Welcome</h1>
 *
 *      <%php $this->part('sidebar'); %>
 *
 *      <!-- goes to the 'sidebar' -->
 *
 *      <%php $this->add('footer_block'); %>
 *
 *      <!-- appends to an array 'footer_block[]' -->
 *
 *      ```
 *
 *    Variables are available through:
 *
 *      `$this->prop('some_name')` as '"this &quot; is escaped"'
 *      `$this->json('some_name')` as '"this \" is escaped"'
 *      `$this->some_name` as htmlspecialchars($string, ENT_QUOTES);
 *
 *    Include another template:
 *
 *      `$this->include('template', ['var' => 'var'])`
 *
 * 4. Sometimes, the template you extend wants an array of values. To send such values to the
 *    parent, you simply have to call `$this->parent->someValue[]`, or use `$this->add('someValue')`
 *    instead of `$this->part('someValue')`.
 *
 * 5. To render a template, simply call:
 *
 *      ```
 *      echo new TPL('path.to.some-template', [ 'variable' => 'value' ]);
 *      ```
 *
 * 6. To use the rendered template elsewhere, for example in an e-mail, call
 *    `$template->render()`.
 *
 *
 */
class TPL {

    public static $template_root = './templates';

    public $template;
    public $vars;
    public $extending = null;
    protected $capturing = null;
    protected $capturingArray = false;
    protected static $didInitialFlush = false;
    protected $wasRendered = false;
    protected static $templateStack = [];

    public function __construct(string $template, $vars=[]) {
        if (!is_array($vars) && !is_object($vars)) {
            throw new TPL\Error("Argument 2 must be an array or an object");
        }
        static::$templateStack[] = $this;
        if (!static::$didInitialFlush) {
            while (sizeof(ob_list_handlers()) > 0) ob_end_flush();
            static::$didInitialFlush = true;
        }
        $this->template = strtr($template, ['.' => '/']);
        if (!file_exists($path = static::$template_root.'/'.$template.'.tpl.php')) {
            clearstatcache(true, $path);
            throw new TPL\Error("Template file '".$path."' not found");
        }
        $this->vars = $vars;
    }

    protected function extend(string $template) {
        if ($this->extending !== null) {
            throw new TPL\Error("Can't extend two templates");
        }
        $this->extending = new static($template, $this->vars);
        $this->extending->vars = &$this->vars;
        $this->part('body');
    }

    public function part(string $partName, string $value=null) {
        if ($this->extending === null) {
            throw new TPL\Error("Can't make part when not extending another template");
        }
        if ($value !== null) {
            $this->extending->vars[$partName] = $value;
        } else {
            if ($this->capturing !== null) {
                // Convenient to not have to call $this->end() after each part.
                $this->end();
            }
            ob_start(null, 0, PHP_OUTPUT_HANDLER_CLEANABLE | PHP_OUTPUT_HANDLER_REMOVABLE);
            $this->capturing = $partName;
            $this->capturingArray = false;
        }
    }

    public function add(string $partName, string $value=null) {
        if ($this->extending === null) {
            throw new TPL\Error("Can't make part when not extending another template");
        }
        if ($value !== null) {
            $this->extending->vars[$partName][] = $value;
        } else {
            if ($this->capturing !== null) {
                // Convenient to not have to call $this->end() after each part.
                $this->end();
            }
            ob_start(null, 0, PHP_OUTPUT_HANDLER_CLEANABLE | PHP_OUTPUT_HANDLER_REMOVABLE);
            $this->capturing = $partName;
            $this->capturingArray = true;
        }
    }

    public function end(): void {
        if ($this->capturing === null) {
            throw new TPL\Error("Can't use \$this->end() without \$this->part() first");
        }
        if ($this->capturingArray) {
            $this->extending->vars[$this->capturing][] = ob_get_contents();
        } else {
            $this->extending->vars[$this->capturing] = ob_get_contents();
        }
        ob_end_clean();
        $this->capturing = null;
        $this->capturingArray = false;
    }

    private $resultBuffer;

    public function __destruct() {
        if (!$this->wasRendered) {
            if (static::$templateStack[0] === $this) {
                echo $this->render();
            }
        }
        array_pop(static::$templateStack);
    }

    public function __set(string $name, $value) {
        return $this->set($name, $value);
    }

    public function __get(string $name) {
        return $this->get($name);
    }

    public function __call(string $name, array $args) {
        if (is_object($this->vars) && method_exists($this->vars, $name)) {
            return call_user_func_array([ $this->vars, $name], $args);
        } elseif (is_array($this->vars) && is_callable($this->vars[$name])) {
            return call_user_func_array($this->vars[$name], $args);
        }
        return "Template method '$name' not found";
    }

    /**
     * Unescaped version of the value
     */
    public function get(string $name) {
        if (is_array($this->vars)) {
            $where = $this->template;
            return $this->vars[$name] ?? '$this->'.$name.' not found (template '.$this->template.')';
        } elseif (is_object($this->vars)) {
            try {
                return $this->vars->$name;
            } catch (\Throwable $e) {
                return $e->getMessage().' (source class '.get_class($this->vars).', template '.$this->template.')';
            }
        }
    }

    public function set(string $name, $value) {
        if (is_array($this->vars)) {
            $this->vars[$name] = $value;
        } elseif (is_object($this->vars)) {
            $this->vars->$name = $value;
        } else {
            $this->vars = [ $name => $value ];
        }
    }

    /**
     * Escaped for use in properties with the quote symbol added.
     */
    public function prop(string $name) {
        return '"'.$this->html($name).'"';
    }

    public function json(string $name) {
        return json_encode($this->get($name));
    }

    public function html(string $name) {
        $val = $this->get($name);
        if ($val === null) {
            return '';
        }
        return htmlspecialchars($val, ENT_QUOTES);
    }

    public function js_prop(string $name) {
        return htmlspecialchars(json_encode($this->get($name)), ENT_QUOTES);
    }

    public function include(string $template, $vars=[]) {
        $template = new self($template, $vars);
        return $template->render();
    }

    public function render() {
 //       extract($this->vars);
        ob_start(null, 0, PHP_OUTPUT_HANDLER_CLEANABLE | PHP_OUTPUT_HANDLER_REMOVABLE);
        include(static::$template_root.'/'.$this->template.'.tpl.php');

        if ($this->extending) {
            if ($this->capturing) {
                $this->end();
            }
            echo $this->extending->render();
            $this->extending = null;
        }

        $result = ob_get_contents();
        ob_end_clean();

        return $result;
    }

    public function __toString(): string {
        return $this->render();
    }
}
